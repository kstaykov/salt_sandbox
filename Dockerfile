FROM debian:7-slim
RUN apt-get update
RUN apt-get install -y curl wget git
RUN curl -L https://bootstrap.saltstack.com | sh
