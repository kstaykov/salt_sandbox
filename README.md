# Salty - the saltstack sandbox

Just for fun and learning.

## Requirements

* Vagrant
* Virtualbox

## Quick start

```
vagrant up
vagrant ssh
sudo su -
salt-call --local state.apply
```
