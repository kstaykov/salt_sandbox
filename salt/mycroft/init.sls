mycroft:
  user.present:
    - fullname: mycroft
    - shell: /bin/bash
    - home: /home/mycroft
    - uid: 4000
    - gid: 100
    - groups:
      - users

/etc/sudoers.d/mycroft:
  file.managed:
    - source: salt://mycroft/mycroft_sudo
    - user: root
    - group: root
    - mode: 440

mycroft-core:
  git.latest:
    - name: https://github.com/MycroftAI/mycroft-core.git
    - target: /home/mycroft/mycroft-core
    - user: mycroft
    - depth: 1
    - rev: master

dev_setup:
  cmd.run:
    - name: /home/mycroft/mycroft-core/dev_setup.sh
    - runas: mycroft
    - cwd: /home/mycroft/mycroft-core